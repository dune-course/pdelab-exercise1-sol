#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>
#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>


/** a local operator for solving the equation
 *
 *   \nabla \cdot ( - K(x) \nabla u ) + a*u = f   in \Omega
 *                  - K(x) \nabla u \cdot n = 0   on \partial\Omega
 *
 * with conforming finite elements on all types of grids in any dimension
 *
 */
class Example01cLocalOperator :
  public Dune::PDELab::NumericalJacobianApplyVolume<Example01cLocalOperator>,
  public Dune::PDELab::NumericalJacobianVolume<Example01cLocalOperator>,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags
{
private:
  unsigned int intorder;


public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };

  Example01cLocalOperator (unsigned int intorder_=2)
    :
    intorder(intorder_)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // extract some types
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int dimw = EG::Geometry::dimensionworld;

    Dune::GeometryType gt = eg.geometry().type();


    // TODO:
    // Begin: Insert your code here to calculate the diffusion coefficient K
    // Evaluate the diffusion coefficient at cell center (in global coodinates!)
    // assume it to be element-wise constant

    Dune::FieldVector<DF,dim> localcenter =
      Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);

    Dune::FieldVector<DF,dim> globalcenter =
      eg.geometry().global( localcenter );

    int mx,my;

    mx = ((int)floor( 8.0 * globalcenter[0] ))%2;
    my = ((int)floor( 8.0 * globalcenter[1] ))%2;
    RF K;
    if ( my==0 && mx==0 ) K = 2.0; // 1.0 * 1E+9;
    if ( my==0 && mx==1 ) K = 0.1; // 1.0 * 1E-6;
    if ( my==1 && mx==0 ) K = 0.5; // 1.0 * 1E-2;
    if ( my==1 && mx==1 ) K = 1.0; // 1.0 * 1E+5;

    // End: Insert


    // select quadrature rule
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    for (const auto& ip : rule)
      {
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(ip.position(),phi);

        // compute u at integration point
        RF u=0.0;
        for (size_type i=0; i<lfsu.size(); i++)
          u += x(lfsu,i)*phi[i];

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateJacobian(ip.position(),js);

        // transform gradients from reference element to real element
        const Dune::FieldMatrix<DF,dimw,dim>
          jac = eg.geometry().jacobianInverseTransposed(ip.position());
        std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
        for (size_type i=0; i<lfsu.size(); i++)
          jac.mv(js[i][0],gradphi[i]);

        // compute gradient of u
        Dune::FieldVector<RF,dim> gradu(0.0);
        for (size_type i=0; i<lfsu.size(); i++)
          gradu.axpy(x(lfsu,i),gradphi[i]);

        // evaluate parameters
        Dune::FieldVector<RF,dim>
          globalpos = eg.geometry().global(ip.position());
        Dune::FieldVector<RF,dim> midpoint(0.5);
        globalpos -= midpoint;
        RF f;
        if (globalpos.two_norm()<0.25) f = -10.0; else f = 10.0;
        RF a =  10.0;

        // integrate K * grad u * grad phi_i + a*u*phi_i - f phi_i
        RF factor = ip.weight()*eg.geometry().integrationElement(ip.position());
        for (size_type i=0; i<lfsu.size(); i++)
          r.accumulate(lfsu,i,( K*(gradu*gradphi[i]) + a*u*phi[i] - f*phi[i] )*factor);
      }
  }

};
